//
//  ItemCollectionViewCell.swift
//  SampleApp
//
//  Created by amir on 10/10/16.
//  Copyright © 2016 nopaad. All rights reserved.
//

import UIKit
import Kingfisher

class ItemCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    func setupCellWithInfo(info:[String:Any]){
    
        
        if let titleText = info["title"] as? String {
            titleLabel.text = titleText
        }
        
        
        if let priceText = info["price"] as? String  {
            priceLabel.text = priceText
        }
        
        if let photoUrl = info["photo_url"] as? String  {
            
            let url = URL(string: photoUrl)
            thumbnailImageView.kf.setImage(with:url)
        }
    
    }
}
