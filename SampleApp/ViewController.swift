//
//  ViewController.swift
//  SampleApp
//
//  Created by amir on 10/10/16.
//  Copyright © 2016 nopaad. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController,UICollectionViewDataSource {

    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var errorOnLoadLabel: UILabel!
    @IBOutlet weak var reloadButton: UIButton!
    
    
    let objectListUrl = "http://shahreketabonline.com/api/product/getBestSellers?categoryId=49&count=20"
    var collectionViewItems:[[String : Any]] = []
    
    
    //MARK: - controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
                 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        prepareLoadAndShowData()
    }
    
    
    //MARK: - load data
    
    
    func prepareLoadAndShowData(){
    
        
        //hide collectionView and show loading
        collectionView.isHidden = true
        indicator.isHidden = false
        errorOnLoadLabel.isHidden = true
        reloadButton.isHidden = true
        
        
        
        loadData { (success:Bool, objectList:[[String : Any]]?) in
            
            //hide loading view
            self.indicator.isHidden = true
            self.reloadButton.isHidden = false
            
            
            if success {
                
                if let objectList = objectList {
                    
                    self.collectionViewItems = objectList
                    self.collectionView.reloadData()
                    self.collectionView.isHidden = false
                    return
                }
            }
            
            //show error
            self.errorOnLoadLabel.isHidden = false
        }
        
    
    }
    
    
    func loadData(complete:@escaping (_ success:Bool, _ objectList:[[String:Any]]?)->()){
    
        
        Alamofire.request(objectListUrl).responseJSON { response in
            
            if let JSON = response.result.value {
                
                if let resultObject = JSON as? [String:Any] {
                
                    if let resultStatus = resultObject["status"] as? String{
                    
                        if resultStatus == "success" {
                            
                            if let data = resultObject["data"] as? [[String:Any]] {
                            
                                complete(true, data)
                                return
                            }
                        }
                    }
                }
            }
            
            complete(false, nil)
        }
        
        
    
    }
    
    

    
    //MARK: - collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath)
        
        if let cell = cell as? ItemCollectionViewCell {
            
            let itemInfo = collectionViewItems[indexPath.row]
            
            cell.setupCellWithInfo(info: itemInfo )
            
            // for smooth scrolling
            cell.contentView.layer.shouldRasterize = true
            cell.contentView.layer.rasterizationScale = UIScreen.main.scale
            
        }
        
        
        return cell
        
    }

    
    //MARK: - button action
    
    @IBAction func reloadButtonTouchUp(_ sender: AnyObject) {
        
        prepareLoadAndShowData()
    }
    
}

